<?php


namespace ShipIT\AsyncRollbar\Providers;


use Illuminate\Support\Facades\File;
use Rollbar\Laravel\RollbarServiceProvider;
use Rollbar\RollbarLogger;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function register()
    {

        parent::register();

        if (!config('logging.channels.rollbar.access_token')) {
            return;
        }

        $this->app->extend(RollbarLogger::class, function (RollbarLogger $logger) {

            $logger->configure(['handler' => 'agent']);

            return $logger;
        });

        $this->makeAgentDir();
        $this->adjustConfig();

        $this->app->register(RollbarServiceProvider::class);
    }

    private function makeAgentDir(): void
    {
        $path = config('logging.channels.rollbar.agent_log_location');

        if (File::exists($path)) {
            return;
        }

        File::makeDirectory($path);
    }

    private function adjustConfig(): void
    {
        $key         = 'logging.channels.stack';
        $stackConfig = $this->app['config'][$key];

        $stackConfig['channels'][] = 'rollbar';

        $this->app['config'][$key] = $stackConfig;
    }


}